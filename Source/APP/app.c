/**************************************************************************************************************
*                                                uC/OS-II
*                                          The Real-Time Kernel
* File : APP.C
* Author: wangsg,Meano
* QQ:591650928,859592326
* Email:www_wsg@163.com,limy@lzrobot.com
**************************************************************************************************************/
#include  <includes.h>

//变量区

//uCOS任务堆栈
OS_STK  AppTaskStartStk[OS_TASK_START_STK_SIZE];
OS_STK  AppTask1Stk[OS_TASK_1_STK_SIZE];
OS_STK  AppTask2Stk[OS_TASK_2_STK_SIZE]; 
OS_STK  AppTask3Stk[OS_TASK_3_STK_SIZE]; 

//uCOS信号量
OS_EVENT* FishUartDataSem;
OS_EVENT* ALIGNSem;
OS_EVENT* TIMER3_Ovf_Sem;

//标志变量
INT8U fReceive = 0;
INT8U ALIGN;

//外部拓展变量
extern INT8U UART0_ARRY[4];
extern INT8U EX_SPEED;
extern INT8U Static_Offset_degree[3];
extern INT8U FISHID;
extern int Joint_Angle_value [3]; 
extern INT8U EX_DIRECTION;

//函数声明
static void  AppTaskStart(void *p_arg);
static void  AppTaskCreate(void);
static void  AppTask1(void *p_arg);
static void  AppTask2(void *p_arg);
static void  AppTask3(void *p_arg);

//主函数
int  main (void)
{
    #if (OS_TASK_NAME_SIZE > 14) && (OS_TASK_STAT_EN > 0)
        INT8U  err;
    #endif

    OSTaskStkSize     = OS_TASK_IDLE_STK_SIZE;                          //设置默认堆栈大小
    OSTaskStkSizeHard = OS_TASK_STK_SIZE_HARD;                          //设置默认硬件堆栈大小
    OSInit();                                                           //初始化"uC/OS-II, 片上实时操作系统" 
    OSTaskStkSize     = OS_TASK_START_STK_SIZE;                         //设置总的堆栈大小
    OSTaskStkSizeHard = OS_TASK_STK_SIZE_HARD;                          //设置默认硬件堆栈大小
    OSTaskCreateExt(AppTaskStart,                                       //任务本体
                    (void *)0,                                          //传递参数
                    (OS_STK *)&AppTaskStartStk[OSTaskStkSize - 1],      //任务堆栈指针
                    OS_TASK_START_PRIO,                                 //任务优先级
                    0,                                                  //无用 id拓展
                    (OS_STK *)&AppTaskStartStk[OSTaskStkSizeHard],      //栈底指针
                    OSTaskStkSize - OSTaskStkSizeHard,                  //堆栈大小
                    (void *)0,                                          //结构指针
                    OS_TASK_OPT_STK_CHK | OS_TASK_OPT_STK_CLR);         //任务操作相关信息

    #if (OS_TASK_NAME_SIZE > 14) && (OS_TASK_STAT_EN > 0)
        OSTaskNameSet(OS_TASK_START_PRIO, "Start Task", &err);          //设置任务名称为Start Task
    #endif
    OSStart();                                                          //系统开始运行
}

//任务创建函数
static  void  AppTaskCreate (void)
{
#if (OS_TASK_NAME_SIZE > 14) && (OS_TASK_STAT_EN > 0)
    INT8U  err;
#endif
    /*---- Task initialization code goes HERE! --------------------------------------------------------*/
    OSTaskStkSize     = OS_TASK_1_STK_SIZE;        /* Setup the default stack size                     */
    OSTaskStkSizeHard = OS_TASK_STK_SIZE_HARD;     /* Setup the default hardware stack size            */
    OSTaskCreateExt(AppTask1,
                    (void *)0,
                    (OS_STK *)&AppTask1Stk[OSTaskStkSize - 1],
                    OS_TASK_1_PRIO,
                    OS_TASK_1_PRIO,
                    (OS_STK *)&AppTask1Stk[OSTaskStkSizeHard],
                    OSTaskStkSize - OSTaskStkSizeHard,
                    (void *)0,
                    OS_TASK_OPT_STK_CHK | OS_TASK_OPT_STK_CLR);
#if (OS_TASK_NAME_SIZE > 14) && (OS_TASK_STAT_EN > 0)
    OSTaskNameSet(OS_TASK_1_PRIO, "Task 1", &err);
#endif

    /*---- Task initialization code goes HERE! --------------------------------------------------------*/
    OSTaskStkSize     = OS_TASK_2_STK_SIZE;        /* Setup the default stack size                     */
    OSTaskStkSizeHard = OS_TASK_STK_SIZE_HARD;     /* Setup the default hardware stack size            */
    OSTaskCreateExt(AppTask2,
                    (void *)0,
                    (OS_STK *)&AppTask2Stk[OSTaskStkSize - 1],
                    OS_TASK_2_PRIO,
                    OS_TASK_2_PRIO,
                    (OS_STK *)&AppTask2Stk[OSTaskStkSizeHard],
                    OSTaskStkSize - OSTaskStkSizeHard,
                    (void *)0,
                    OS_TASK_OPT_STK_CHK | OS_TASK_OPT_STK_CLR);
#if (OS_TASK_NAME_SIZE > 14) && (OS_TASK_STAT_EN > 0)
    OSTaskNameSet(OS_TASK_2_PRIO, "Task 2", &err);
#endif
        /*---- Task initialization code goes HERE! --------------------------------------------------------*/
    OSTaskStkSize     = OS_TASK_3_STK_SIZE;        /* Setup the default stack size                     */
    OSTaskStkSizeHard = OS_TASK_STK_SIZE_HARD;     /* Setup the default hardware stack size            */
    OSTaskCreateExt(AppTask3,
                    (void *)0,
                    (OS_STK *)&AppTask3Stk[OSTaskStkSize - 1],
                    OS_TASK_3_PRIO,
                    OS_TASK_3_PRIO,
                    (OS_STK *)&AppTask3Stk[OSTaskStkSizeHard],
                    OSTaskStkSize - OSTaskStkSizeHard,
                    (void *)0,
                    OS_TASK_OPT_STK_CHK | OS_TASK_OPT_STK_CLR);
#if (OS_TASK_NAME_SIZE > 14) && (OS_TASK_STAT_EN > 0)
    OSTaskNameSet(OS_TASK_3_PRIO, "Task 3", &err);
#endif
}

//任务0，任务初始化，包含系统主任务
static void  AppTaskStart (void *p_arg)
{
    INT8U error;                                                        //防止编译警告
    BSP_Init();                                                         //时钟初始化
    TWI_Master_Initialise();                                            //IIC初始化
    USART0Init();                                                       //串口0初始化
    Init_Devices();                                                     //片上硬件初始化
    AppTaskCreate();                                                    //创建更多任务
    FishUartDataSem = OSSemCreate(0);                                   //串口接收完成信号量
    TIMER3_Ovf_Sem= OSSemCreate(0);                                     //定时器3溢出，信号量
    ALIGNSem = OSSemCreate(0);                                          //接收调直命令信号量
    Init_RobotFish_Data();                                              //机器鱼数据初始化，读出外部存储器中的数据
    EX_DIRECTION=0x07;                                                  //初始状态在中间位置
    while (1) {    	
        OSSemPend(FishUartDataSem,0,&error);                            //等待串口0发送完成信号 
        if((fReceive==2)&&((UART0_ARRY[1]&0x0F)==FISHID))
        {
            switch (UART0_ARRY[2]&0Xf0)
            {	  
                case 0xd0:      //speed
                    EX_SPEED = (UART0_ARRY[2]&0x0f);//
                    break;
                case 0Xe0:      //direction
                    EX_DIRECTION = (UART0_ARRY[2]&0x0f);
                    break;
                case 0x00:      //align
                    ALIGN = (UART0_ARRY[2]&0x0f);
                    OSSemPost(ALIGNSem);
                    break;
                case 0X10:      // keep the align
                    if(UART0_ARRY[2]==0x1C)
                    {
                        fish_write_align_data();
                        for(INT8U i=0;i<3;i++)
                        {
                            USART0_Transmit(Static_Offset_degree[i]);
                        }
                    }
                    else if(UART0_ARRY[2]==0x18)
                    {
                        fish_reset_align_data();
                        for(INT8U i=0;i<3;i++)
                        {
                            USART0_Transmit(Static_Offset_degree[i]);
                        }
                    }
                    //USART0_Transmit('\n');
                    break;
                case 0x30:
                    if(UART0_ARRY[2]==0x33)
                    {
                        __watchdog_reset();     //看门狗计数清零
                        WDTCR = 0x18;           //允许修改WDTCR寄存器
                        WDTCR = 0x08;           //16K分频14.8ms后程序复位
                    }
                break;
                case 0x90:
                    if(UART0_ARRY[2]==0x99)
                    {
                        system_write_i2c_data();//恢复出厂设置
                        system_read_i2c_data();
                        USART0_Transmit(99);
                    }
                break;
                default:
                    break;
            }
        }
        fReceive=0;
        IO_Toggle(B,2);//知识灯闪烁
    }
}

//任务1，调直任务
static void  AppTask1(void *p_arg) 
{
    (void)p_arg;
    INT8U error;
    while (1) {
        OSSemPend(ALIGNSem,0,&error);               //等待调直信号量
        INT8U tem;
        if(error != OS_ERR_NONE) return;
        tem = ALIGN&0X07;   
        if((ALIGN&0X08)==0x00)
        {
            if(Static_Offset_degree[tem]>= 120)
            {
                Static_Offset_degree[tem]--;		
            }
            Static_Offset_degree[tem]++;
        }
        else
        {
            if(Static_Offset_degree[tem] <= 0)
            {
                Static_Offset_degree[tem]++;		
            }
            Static_Offset_degree[tem]--;
        }
        USART0_Transmit(Static_Offset_degree[tem]);
    }
}

//任务2，舵机角度计算任务
static void  AppTask2(void *p_arg)
{
    (void)p_arg;
    INT8U error;
    while (1) {
        OSSemPend(TIMER3_Ovf_Sem,0,&error);
        if(fReceive<2){
            fReceive++;
        }else if(fReceive>2){
            fReceive=2;
        }
        calculate_data();
        OCR1A=Joint_Angle_value[0];	
        OCR1B=Joint_Angle_value[1];
        OCR3A=Joint_Angle_value[2]; 
    }
}

//任务3，用户拓展任务
static void  AppTask3(void *p_arg)
{
    (void)p_arg;
    while (1) {
        //在此拓展任务3，必须使用OSTimeDly(delaytime)作为延时函数，且不能使用死循环
        OSTimeDly(1);                   //延时5ms,uCOS任务中一定要添加延时或信号等待，否则程序会卡死
    }
}
